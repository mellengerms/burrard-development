var Project_Links = function(data) {
	data = typeof data !== 'object' ? {} : data;
	for (var i in data) {
		this[i] = data[i];
	}

	this.attr = typeof this.attr === 'undefined' ? 'data-project-link' : this.attr;

	this.init();
};

Project_Links.prototype = {
	eta: 300,
	modal: 'project-modal',
	init: function() {
		var Obj = this;
		$('[' + Obj.attr + ']').click(function(e) {
			// e.preventDefault();
			var url = $(this).attr(Obj.attr);
			Obj.load(url);
		});

		Hashbang.map(['<page>','<sub>','<proj>'], function() {
			if (this.sub == 'project') {
                var D_L = new Data_Links();
                D_L.scroll_to([this.page, this.sub, this.proj]);
                Obj.load(this.proj);
			}
		});
	},
	load: function(url) {
		var Obj = this;
		$.ajax({
			url: '/inc/projects/' + url + '.php',
			success: function(res) {
				res = $('<div class="' + Obj.modal + '-inner">' + res + '</div>').click(function(e) {
					e.stopPropagation();
				});

				$('<div id="' + Obj.modal + '"></div>').appendTo('body');
				
				$('#' + Obj.modal + '')
					.append(res)
					.click(function(e) {
						$('#' + Obj.modal).fadeOut(Obj.eta,function() {
							$(this).remove();
                            history.pushState({},'Page shift','#!/projects');
                        });
					})
					.hide()
					.fadeIn(Obj.eta);
			},
			failure: function(res) {
				console.log(res);
			},
		});
	},
};