var Data_Links = function(data) {
	data = typeof data !== 'object' ? {} : data;
	for (var i in data) {
		this[i] = data[i];
	}

	this.attr = typeof this.attr === 'undefined' ? 'data-link' : this.attr;

	this.init();
};

Data_Links.prototype = {
	eta: 300,
	init: function() {
		var Obj = this;
		$('[' + Obj.attr + ']').click(function(e) {
			e.preventDefault();
			var url = $(this).attr(Obj.attr);
			url = url.split('/');
			Obj.scroll_to(url);
		});

		Hashbang.map(['<page>','<sub>',], function() {
			Obj.scroll_to([this.page,this.sub]);
		});

		Hashbang.map(['<page>'], function() {
			Obj.scroll_to([this.page]);
		});		
	},
	highlight: function(url) {
		var Obj = this;
		$('[' + Obj.attr + '].js-active').removeClass('js-active');
		$('[' + Obj.attr + '="' + url[0] + '"], [' + Obj.attr + '="' + url.join('/') + '"]').addClass('js-active');
	},
	change_url: function(url) {
		console.log
		history.pushState({},'Page shift','#!/' + url.join('/'));
	},
	scroll_to: function(url) {
		var Obj = this;
		
		Obj.highlight(url);
		Obj.change_url(url);

		if (!($('[data-slide="' + url[0] + '"]').hasClass('js-active'))) {
			$('[data-slide].js-active').removeClass('js-active').addClass('js-fading');
			$('[data-slide="' + url[0] + '"]').addClass('js-active');
			Obj.t = setTimeout(function() {
				$('[data-slide].js-fading').removeClass('js-fading');
			},Obj.eta);
		}
	},
};