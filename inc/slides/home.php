<div class="slide js-active" data-slide="home">
  <div class="inner">
    <div id="home-gallery">
      <div class="home-gallery-item" style="background-image: url(/img/slide-1.jpg);" data-link="about">
        <div class="inner">
          <h6>Nexus</h6>
          <h2>Ahead of the Game</h2>
        </div>
      </div>
      <div class="home-gallery-item" style="background-image: url(/img/slide-2.jpg);" data-link="about">
        <div class="inner">
            <h6>Chan Center</h6>
          <h2>A Sound Experience</h2>
        </div>
      </div>
      <div class="home-gallery-item" style="background-image: url(/img/slide-3.jpg);" data-link="about">
        <div class="inner">
          <h6>Some Things</h6>
          <h2>Are Made Up Content</h2>
        </div>
      </div>
    </div>
  </div>
</div>