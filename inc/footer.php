<footer id="footer">
  <div class="footer-left">
    <div class="footer-left-col">
      <a href="#" data-link="home"><em>Burrard Development</em></a>
    </div>
    <div class="footer-left-col">
      VANCOUVER<br />
      1030 West Georgia Street<br />
      Vancouver, BC<br />
      Canada V6E 2Y3
    </div>
    <div class="footer-left-col">
      SEATTLE<br />
      Suite 100 &ndash; 81 Vine Street<br />
      Seattle, WA<br />
      United States 90210
    </div>
    <div class="footer-left-col">
      +1(604) 616-0453<br />
      <a href="mailto:contact@burrard.com">contact@burrard.com</a>    
    </div>
  </div>
  <div class="footer-right">
    @ 2017 Burrard Development. All Rights Reserved
  </div>
</footer>