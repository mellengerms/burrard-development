<header id="header">
  <div id="header-logo">LOGO</div>
  <ul id="header-menu">
    <li><a href="#" data-link="home">Home</a></li>
    <li><a href="#" data-link="about">About Us</a></li>
    <li><a href="#" data-link="projects">Projects</a></li>
    <li><a href="#" data-link="contact">Contact</a></li>
  </ul>
</header>