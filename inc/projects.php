<?php 
  $proj = array(
    array(
      'title' => 'Project X',
      'link' => array(
        'url' => 'project-x',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage.',
    ),
    array(
      'title' => 'Project Y',
      'link' => array(
        'url' => 'project-y',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents. Typewriter knausgaard locavore whatever chartreuse. Godard kogi etsy intelligentsia, mlkshk pok pok photo booth migas la croix drinking vinegar fashion axe XOXO hell of taxidermy cardigan. ',
    ),
    array(
      'title' => 'Project Z',
      'link' => array(
        'url' => 'project-z',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage. Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents.',
    ),    array(
      'title' => 'Project N',
      'link' => array(
        'url' => 'project-n',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage.',
    ),
    array(
      'title' => 'Project M',
      'link' => array(
        'url' => 'project-m',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents. Typewriter knausgaard locavore whatever chartreuse. Godard kogi etsy intelligentsia, mlkshk pok pok photo booth migas la croix drinking vinegar fashion axe XOXO hell of taxidermy cardigan. ',
    ),
    array(
      'title' => 'Project L',
      'link' => array(
        'url' => 'project-l',
        'text' => 'See more',
      ),
      'image' => '/img/slide-3.jpg',
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage. Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents.',
    ),    array(
      'title' => 'Project C',
      'image' => '/img/slide-3.jpg',
      'link' => array(
        'url' => 'project-c',
        'text' => 'See more',
      ),
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage.',
    ),
    array(
      'title' => 'Project B',
      'image' => '/img/slide-3.jpg',
      'link' => array(
        'url' => 'project-b',
        'text' => 'See more',
      ),
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents. Typewriter knausgaard locavore whatever chartreuse. Godard kogi etsy intelligentsia, mlkshk pok pok photo booth migas la croix drinking vinegar fashion axe XOXO hell of taxidermy cardigan. ',
    ),
    array(
      'title' => 'Project A',
      'image' => '/img/slide-3.jpg',
      'link' => array(
        'url' => 'project-a',
        'text' => 'See more',
      ),
      'sub' => 'San Fransisco /<br /> Development',
      'body' => 'Thundercats gluten-free occupy fixie pinterest edison bulb, hot chicken chillwave tumblr tattooed everyday carry cray heirloom. Hammock kinfolk portland williamsburg tumeric retro. Godard post-ironic ennui tattooed butcher waistcoat selvage. Narwhal portland shoreditch, fam venmo woke deep v quinoa whatever chia offal readymade vinyl succulents.',
    ),
  );
?>

<?php foreach ($proj as $key => $val) {
  $tmp_url = isset($val['link']); 
  
  $tmp_bottom = '';
  if ($tmp_url) {
    $tmp_button = '
      <div class="project-link">
        <a class="button" href="#" data-project-link="project-link'.$val['link']['url'].'">'.$val['link']['text'].'</a>
      </div>';
  }

  $tmp_sub = '';
  if (isset($val['sub'])) {
    $tmp_sub = '
      <h5 class="project-sub">'.$val['sub'].'</h5>';
  }

  echo '
  <div class="project" '.(isset($val['image']) ? 'style="background-image: url('.$val['image'].');"' : '').'>
    <h3 class="project-title">'.($tmp_url ? '<a href="#" data-link="projects/project/'.$val['link']['url'].'" data-project-link="'.$val['link']['url'].'">'.$val['title'].'</a>' : $val['title']).'</h3>
    '.$tmp_sub.'
    <!--
    <div class="project-body">'.$val['body'].'</div>
    '.$tmp_button.'
    -->
  </div>';
} ?>